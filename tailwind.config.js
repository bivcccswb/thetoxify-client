const colors = require('tailwindcss/colors');

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      minHeight: {
        '80': '20rem',
      },
      colors: {
        sky: colors.sky,
        cyan: colors.cyan,
      },
      borderRadius: {
        'large': '12px',
      },
      boxShadow: {
        'card': '0 2px 8px rgba(0, 0, 0, 0.26)',
      },
      backgroundImage: theme => ({
        'hero-background': "url('./assets/background.jpeg')",
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
