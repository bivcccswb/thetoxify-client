import { createRouter, createWebHistory } from 'vue-router';

import CreateBlog from './components/blogs/CreateBlog.vue';
import EditBlog from './components/blogs/EditBlog.vue';
import AllBlogs from './components/blogs/AllBlogs.vue';
import Blog from './components/blogs/Blog.vue';
import Profile from './components/profile/Profile.vue';
import NotFound from './components/NotFound.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [{
        path: '/',
        redirect: '/blogs',
    }, {
        path: '/createblog',
        component: CreateBlog,
    }, {
        path: '/blogs',
        component: AllBlogs,
    }, {
        path: '/blogs/:id',
        component: Blog,
        props: true,
    }, {
        path: '/blogs/:id/edit',
        component: EditBlog,
        props: true,
    }, {
        path: '/myprofile',
        component: Profile,
    }, {
        path: '/:notFound(.*)',
        component: NotFound,
    },],
});

export default router;