import util from './../../../util/utility';
import Constants from './../../../util/constants';

export default {
  async setUser(context, payload) {
    let response = await fetch(`${Constants.BASE_URL}/users/${payload.email.replace(/[^a-z0-9@]/gi, '_')}.json`);
    if (!response.ok) {

    }
    let user = await response.json();
    if (!user) {
      let { accessToken, ...user } = payload;
      user = {
        ...user,
        id: util.getUniqueUserId(),
      };
      response = await fetch(`${Constants.BASE_URL}/users/${payload.email.replace(/[^a-z0-9@]/gi, '_')}.json`, {
        method: "PUT",
        body: JSON.stringify(user),
      });
      if (!response.ok) {

      }
    }
    user = {
      ...user,
      accessToken: payload.accessToken,
    }
    context.commit("saveUser", user);
  },
  logout(context) {
    context.commit("deleteUser");
  },
};