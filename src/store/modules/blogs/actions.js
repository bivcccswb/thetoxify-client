import Constants from './../../../util/constants';

export default {
  async fetchBlogs(context) {
    let response = await fetch(`${Constants.BASE_URL}/blogs.json`);
    if (!response.ok) {

    }
    let payload = await response.json();
    let blogs = [];
    if (payload) {
      blogs = Object.values(payload);
    }
    context.commit('fetchBlogs', blogs);
  },
  async fetchBlog(context, blogId) {
    let selectedBlog = context.state.blogs.find(
      (blog) => blog.id == blogId
    );
    if (!selectedBlog) {
      let response = await fetch(`${Constants.BASE_URL}/blogs/${blogId}.json`);
      if (!response.ok) {

      }
      selectedBlog = await response.json();
    }
    context.commit("setSelectedBlog", selectedBlog);
  },
  async createBlog(context, payload) {
    let token = context.rootState.users.currentUser.accessToken;
    let response = await fetch(`${Constants.BASE_URL}/blogs/${payload.id}.json?auth=${token}`, {
      method: 'PUT',
      body: JSON.stringify(payload)
    });
    if (!response.ok) {

    }
    context.commit('createBlog', payload);
  },
  updateLike(context, id) {
    // Do the server side call for updating likes
    const payload = {
      id
    };
    context.commit('updateLike', payload);
  },
  updateDislike(context, id) {
    // Do the server side call for updating dislikes
    const payload = {
      id
    };
    context.commit('updateDislike', payload);
  },
};